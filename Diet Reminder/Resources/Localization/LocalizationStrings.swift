//
//  LocalizationStrings.swift
//  Diet Reminder
//
//  Created by apple on 12/09/18.
//  Copyright © 2018 Aakash Srivastav. All rights reserved.
//

import Foundation

let K_APP_NAME = "K_APP_NAME"

// Alert
let K_PERMISSION_DENIED = "K_PERMISSION_DENIED"
let K_SETTINGS = "K_SETTINGS"
let K_OK = "K_OK"
let K_SUCCESS = "K_SUCCESS"
let K_ERROR = "K_ERROR"

// Week Days
let K_SUNDAY = "K_SUNDAY"
let K_MONDAY = "K_MONDAY"
let K_TUESDAY = "K_TUESDAY"
let K_WEDNESDAY = "K_WEDNESDAY"
let K_THURSDAY = "K_THURSDAY"
let K_FRIDAY = "K_FRIDAY"
let K_SATURDAY = "K_SATURDAY"

// Diet Plan VC
let K_SET_REMINDER = "K_SET_REMINDER"
let K_REMIDER_SCHEDULED_MESSAGE = "K_REMIDER_SCHEDULED_MESSAGE"
let K_ALLOW_ACCESS_TO_REMIDERS_MESSAGE = "K_ALLOW_ACCESS_TO_REMIDERS_MESSAGE"
