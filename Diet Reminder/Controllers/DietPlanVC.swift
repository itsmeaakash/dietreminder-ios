//
//  DietPlanVC.swift
//  Diet Reminder
//
//  Created by Aakash Srivastav on 12/09/18.
//  Copyright © 2018 Aakash Srivastav. All rights reserved.
//

import EventKit
import UIKit

class DietPlanVC: UIViewController {

    // MARK: Private Properties
    private var dietPlan: DietPlan!
    private lazy var eventStore = EKEventStore()
    private var calendar: EKCalendar!
    
    private let kEventStoreKey = "CALENDAR_IDENTIFIER"
    private let kEventSourceKey = "EVENT_SOURCE"

    // MARK: IBOutlets
    @IBOutlet weak var dietTableView: UITableView!
    
    // MARK: View Controller Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //loadJson()
        getDiets()
        
        dietTableView.dataSource = self
        dietTableView.delegate = self
        dietTableView.tableFooterView = UIView()
        
        let setReminderItem = UIBarButtonItem(title: K_SET_REMINDER.localized(), style: .plain, target: self, action: #selector(setReminderTapped))
        self.navigationItem.rightBarButtonItem = setReminderItem
    }
    
    // MARK: Private Methods
    @objc private func setReminderTapped(_ sender: UIBarButtonItem) {
        eventStore.requestAccess(to: .reminder) { (granted, error) in
            if !granted {
                DispatchQueue.main.async {
                    self.showPermissionDeniedAlert()
                }
                return
            }
            if let error = error {
                DispatchQueue.main.async {
                    self.showError(error)
                }
                return
            }
            self.setCalendar()
        }
    }
    
    private func getDietDate(for section: Int) -> [Diet]? {
        guard let dietPlan = dietPlan else {
            return nil
        }
        switch section {
        case 0:
            return dietPlan.weekDietData.sunday
        case 1:
            return dietPlan.weekDietData.monday
        case 2:
            return dietPlan.weekDietData.tuesday
        case 3:
            return dietPlan.weekDietData.wednesday
        case 4:
            return dietPlan.weekDietData.thursday
        case 5:
            return dietPlan.weekDietData.friday
        case 6:
            return dietPlan.weekDietData.saturday
        default:
            fatalError("Section not allowed")
        }
    }
    
    private func getHeaderText(for section: Int) -> String? {
        switch section {
        case 0:
            return K_SUNDAY.localized()
        case 1:
            return K_MONDAY.localized()
        case 2:
            return K_TUESDAY.localized()
        case 3:
            return K_WEDNESDAY.localized()
        case 4:
            return K_THURSDAY.localized()
        case 5:
            return K_FRIDAY.localized()
        case 6:
            return K_SATURDAY.localized()
        default:
            return nil
        }
    }
}

// MARK: Alert Methods
extension DietPlanVC {
    
    private func showError(_ error: Error) {
        let okAction = UIAlertAction(title: K_OK.localized(), style: .default, handler: nil)
        showAlert(with: K_ERROR.localized(),
                  message: error.localizedDescription,
                  actions: [okAction])
    }
    
    private func showPermissionDeniedAlert() {
        let okAction = UIAlertAction(title: K_OK.localized(), style: .default, handler: nil)
        
        let settingsAction = UIAlertAction(title: K_SETTINGS.localized(), style: .cancel, handler: { _ in
            if let settingsUrl = URL(string: UIApplication.openSettingsURLString),
                UIApplication.shared.canOpenURL(settingsUrl){
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            }
        })
        
        showAlert(with: K_PERMISSION_DENIED.localized(),
                  message: K_ALLOW_ACCESS_TO_REMIDERS_MESSAGE.localized(),
                  actions: [okAction, settingsAction])
    }
    
    private func showReminderScheduledAlert() {
        let okAction = UIAlertAction(title: K_OK.localized(), style: .default, handler: nil)
        showAlert(with: K_SUCCESS.localized(),
                  message: K_REMIDER_SCHEDULED_MESSAGE.localized(),
                  actions: [okAction])
    }
    
    private func showAlert(with title: String?, message: String?, actions: [UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        actions.forEach { action in
            alert.addAction(action)
        }
        present(alert, animated: true, completion: nil)
    }
}

// MARK: Calendar Methods
extension DietPlanVC {
    
    private func setCalendar() {
        let eventCalenders = eventStore.calendars(for: .reminder)
        let calendarIdentifier = (UserDefaults.standard.object(forKey: kEventStoreKey) as? String ?? "")
        
        for eventCalender in eventCalenders {
            if eventCalender.calendarIdentifier == calendarIdentifier {
                self.calendar = eventCalender
                break
            }
        }
        
        guard self.calendar == nil else {
            addReminders()
            return
        }
        
        let calendar = EKCalendar(for: .reminder, eventStore: self.eventStore)
        let appName = Bundle.main.infoDictionary?["CFBundleName"] as? String
        calendar.title = appName ?? K_APP_NAME.localized()
        
        let sourceIdentifier = (UserDefaults.standard.object(forKey: kEventSourceKey) as? String ?? "")
        var calendarSource: EKSource?
        
        for source in eventStore.sources {
            if source.sourceIdentifier == sourceIdentifier {
                calendarSource = source
            }
        }
        if let source = calendarSource {
            calendar.source = source
            
        } else if let defaultCalendar = eventStore.defaultCalendarForNewReminders() {
            calendar.source = defaultCalendar.source
            UserDefaults.standard.set(calendar.source.sourceIdentifier, forKey: kEventSourceKey)
            
        } else {
            for source in eventStore.sources {
                if source.sourceType == .local {
                    calendar.source = source
                    break
                }
            }
            if let source = calendar.source {
                UserDefaults.standard.set(source.sourceIdentifier, forKey: kEventSourceKey)
            }
        }
        do {
            try eventStore.saveCalendar(calendar, commit: true)
            UserDefaults.standard.set(calendar.calendarIdentifier, forKey: kEventStoreKey)
            self.calendar = calendar
            
        } catch let error {
            print("error: \(error)")
        }
        
        addReminders()
    }
    
    private func removePreviousReminders() {
        let predicate = eventStore.predicateForReminders(in: [calendar])
        
        eventStore.fetchReminders(matching: predicate) { reminders in
            
            guard let unwrappedReminders = reminders, !unwrappedReminders.isEmpty else {
                return
            }
            
            unwrappedReminders.forEach({ reminder in
                do {
                    try self.eventStore.remove(reminder, commit: true)
                } catch let error {
                    print("error \(error.localizedDescription)")
                }
            })
        }
    }
    
    private func addReminders() {
        
        guard let dietPlan = dietPlan else {
            return
        }
        removePreviousReminders()
        
        let startDate = Date()
        let endDate = Calendar.current.date(byAdding: .day, value: dietPlan.dietDuration, to: startDate)!
        let calenderUnit : Set<Calendar.Component> = [.year, .month, .day, .hour, .minute]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        [Int](0...6).forEach { day in
            
            guard let dietPlan = getDietDate(for: day), !dietPlan.isEmpty else {
                return
            }
            
            dietPlan.forEach({ diet in
                
                let reminder = EKReminder(eventStore: eventStore)
                reminder.calendar = calendar
                reminder.title = diet.food
                
                if let dietDate = dateFormatter.date(from: diet.mealTime) {
                    
                    var dateComponents = DateComponents()
                    dateComponents.year = Calendar.current.component(.year, from: startDate)
                    dateComponents.month = Calendar.current.component(.month, from: startDate)
                    dateComponents.day = Calendar.current.component(.day, from: startDate)
                    dateComponents.hour = Calendar.current.component(.hour, from: dietDate)
                    dateComponents.minute = Calendar.current.component(.minute, from: dietDate)
                    
                    let todaysDietDate = Calendar.current.date(from: dateComponents)!
                    let alarmDate = Calendar.current.date(byAdding: .minute, value: -5, to: todaysDietDate)!
                    let alarm = EKAlarm(absoluteDate: alarmDate)
                    reminder.addAlarm(alarm)
                    
                    reminder.startDateComponents = Calendar.current.dateComponents(calenderUnit, from: todaysDietDate)
                    reminder.dueDateComponents = Calendar.current.dateComponents(calenderUnit, from: todaysDietDate)
                }
                
                var daysOfWeek = [EKRecurrenceDayOfWeek]()
                
                if let weekday = EKWeekday(rawValue: (day + 1)) {
                    daysOfWeek.append(EKRecurrenceDayOfWeek(weekday))
                }
                
                let recurrenceRule = EKRecurrenceRule(recurrenceWith: .weekly, interval: 1, daysOfTheWeek: daysOfWeek, daysOfTheMonth: nil, monthsOfTheYear: nil, weeksOfTheYear: nil, daysOfTheYear: nil, setPositions: nil, end: EKRecurrenceEnd(end: endDate))
                
                reminder.recurrenceRules = [recurrenceRule]
                
                do {
                    try eventStore.save(reminder, commit: true)
                } catch {
                    showError(error)
                }
            })
        }
        
        DispatchQueue.main.async {
            self.showReminderScheduledAlert()
        }
    }
}

// MARK: Fetch Data Methods
extension DietPlanVC {
    
    // Load JSON from file
    private func loadJson() {
        guard let url = Bundle.main.url(forResource: "DietPlan", withExtension: "json") else {
            fatalError("Failed to fetch Diet Plan from json file")
        }
        do {
            let data = try Data(contentsOf: url)
            parseDietData(data)
        } catch {
            fatalError("error: \(error)")
        }
    }
    
    // Parse Data
    private func parseDietData(_ data: Data) {
        let decoder = JSONDecoder()
        do {
            let dietPlan = try decoder.decode(DietPlan.self, from: data)
            self.dietPlan = dietPlan
            self.dietTableView.reloadData()
        } catch {
            fatalError("error: \(error)")
        }
    }
    
    // Service Request
    private func getDiets() {
        
        guard let url = URL(string: "https://naviadoctors.com/dummy/") else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
            
            guard let strongSelf = self else {
                return
            }
            
            if let error = error {
                DispatchQueue.main.async {
                    strongSelf.showError(error)
                }
                
            } else if let data = data {
                DispatchQueue.main.async {
                    strongSelf.parseDietData(data)
                }
            }
        }
        task.resume()
    }
}

// MARK: Table View Data Source Methods
extension DietPlanVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dietDate = getDietDate(for: section), !dietDate.isEmpty {
            return dietDate.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MealTableCell.identifier, for: indexPath) as? MealTableCell else {
            fatalError("MealTableCell cannot be initialized")
        }
        
        if let diet = getDietDate(for: indexPath.section)?[indexPath.row] {
            cell.populate(with: diet)
        }
        
        return cell
    }
}

// MARK: Table View Delegate Methods
extension DietPlanVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let dietDate = getDietDate(for: section), !dietDate.isEmpty {
            return 30
        }
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let labelOriginX: CGFloat = 15
        let label = UILabel(frame: CGRect(x: labelOriginX, y: 0, width: (tableView.frame.width - labelOriginX), height: 30))
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = .white
        label.text = getHeaderText(for: section)
        
        let headerView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.frame.width, height: 30)))
        headerView.addSubview(label)
        headerView.clipsToBounds = true
        headerView.backgroundColor = .lightGray
        
        return headerView
    }
}
