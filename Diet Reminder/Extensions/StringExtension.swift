//
//  StringExtension.swift
//  Diet Reminder
//
//  Created by apple on 12/09/18.
//  Copyright © 2018 Aakash Srivastav. All rights reserved.
//

import Foundation

extension String {
    
    func localized(comment: String = "") -> String {
        return NSLocalizedString(self, comment: comment)
    }
}
